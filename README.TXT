================================================================================

This module is for developer use only!!
And is currently in testing phase!

THIS MODULE IS SUPPOSED TO BE HACKED!

-------------------------------------------------------------------------------
USE
  - Install
  - Write a module which you wish to send email
  - Go to jwl_mail_themer_api/templates\base_template.tpl.php
      - And style this page
  - Run the function
    - jwl_mail_themer_api_send()
        * @param  {string}  $to               // who is it going to.
        * @param  {string}  $subject          // what is the subject.
        * @param  {string}  $key              // Not supported yet (please wait).
        * @param  {string}  $content_theme    // The name of the theme in your module.
        * @param  {string}  $content_vars     // The variables you want to pass to your theme in your module.
        * @param  {boolean} $msg              // Do you want to display a message when the email has been sent.
        * @param  {boolean} $debugs           // if set to true it will die the email so you can style it.

-------------------------------------------------------------------------------

Example
  - Open      :: jwl_mail_themer_api.module
  - Goto line 144
    - If this doesn't make sense (I don't know what to say :( )
-------------------------------------------------------------------------------


REQUIREMENTS
    - MIMEMAIL (https://www.drupal.org/project/mimemail)
-------------------------------------------------------------------------------

By Jake Lacey (jakelacey.com)
